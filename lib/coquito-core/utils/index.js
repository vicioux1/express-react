function parseData(obj, options = {}) {
  return (typeof obj.toJSON === 'undefined' ? obj : obj.toJSON(options));
}

export default {
  parseData,
}