import bcrypt from 'bcrypt';
import utils from '../../utils';

export default async (ctx, emailAddress, password) => {
  let user = {
    emailAddress,
    password
  }
  try {
    const userFound = await findByEmailAndSetPlainPassword(ctx, user);
    await verifyCredentials(userFound);
    return userFound;
  } catch (e) {
    throw new Error("coquito-core.errors.account.login");
  }
}

async function findByEmailAndSetPlainPassword(ctx, user) {
  try {
    const userFound = await ctx.userBucket.findByEmail(user.emailAddress);
    const userParsed = await utils.parseData(userFound, { visibility: false });
    userParsed.plainPassword = user.password;
    return userParsed
  } catch (e) {
    throw e;
  }
}

async function verifyCredentials(user) {
  const match =  await bcrypt.compare(user.plainPassword, user.password);
  if(!match){    
    throw new Error();
  }
  return user;
}
