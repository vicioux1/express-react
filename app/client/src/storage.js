
const getUser = () => {
  return JSON.parse(getItem("user"));
}

const setUser = (user) => {
  setItem("user", JSON.stringify(user));
}

const logoutUser = () => {
  deleteItem("user");
}

const setItem = (key, item) =>{
  localStorage.setItem(key, item);
}

const getItem = key =>{
  return localStorage.getItem(key)
}

const deleteItem = key => {
  localStorage.removeItem(key);
}

export default {
  getUser,
  setUser,
  logoutUser
}