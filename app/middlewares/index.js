import { Router } from 'express';
import vendor from './vendor';

export default function (appContext) {
  const router = Router();

  // must be the first
  router.use(vendor(appContext));

  // here custom middlewares
  // router.use(setBodyLanguage(appContext));

  return router;
}
