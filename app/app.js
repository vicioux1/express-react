import path from 'path';
import express from 'express';
import initializers from './initializers';
import buildMiddlewares from './middlewares';
import errorHandling from './middlewares/error-handling';
import buildRoutes from './router';

const app = express();

// initializers
app.locals.appContext = initializers;

// middlewares
app.use(buildMiddlewares(app.locals.appContext));

// api router
app.use('/', buildRoutes(app.locals.appContext));

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));
// Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

// must be the last
app.use(errorHandling(app.locals.appContext));

export default app;
