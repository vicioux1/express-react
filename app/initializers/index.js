import configuration from '../../config/environment';
import passport from './passport';
import coreServices from './coquito-core';

const config = configuration(process.env);

const initializer = {
  config,
  passport: passport(config.app),
  services: coreServices(config.coquito),
};

export default initializer;