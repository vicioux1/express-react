export default {
  get(req, res /* , next */) {
    const { config } = req.app.locals.appContext;
    const isProd = config.environment === 'production';

    const response = { version: config.app.version };

    if (!isProd) {
      response.environment = config.environment;
    }
    res.json(response);
  },
  list(req, res){
    var list = ["item2", "item3", "item3"];
    res.json(list);
  }
};
