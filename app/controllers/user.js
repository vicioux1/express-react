export default {
  async get({ app, query }, res, next) {
    const service = app.locals.appContext.services.user;
    const { id } = query;
    try {
      const response = await service.find(id);
      const resp = { users: response }
      res.json(resp);
    } catch (e) {
      next(e);
    }
  },
};
